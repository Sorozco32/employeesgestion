<?php

namespace App\Models\Pais;

use App\Models\Empleados\Empleados;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;

    protected $table = 'pais_empleo';

    // Relacion oneToOne 
    public function empleado(){
        return $this->hasOne(Empleados::class);
    }

}
