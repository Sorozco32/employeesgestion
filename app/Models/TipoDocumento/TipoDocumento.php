<?php

namespace App\Models\TipoDocumento;

use App\Models\Empleados\Empleados;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    use HasFactory;

    protected $table = 'tipo_documento';

    // Relacion oneToOne 
    public function empleado(){
        return $this->hasOne(Empleados::class);
    }
}
