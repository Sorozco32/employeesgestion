<?php

namespace App\Models\AreaTrabajo;

use App\Models\Empleados\Empleados;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaTrabajo extends Model
{
    use HasFactory;

    protected $table = 'area_trabajo';

     // Relacion oneToOne 
     public function empleado(){
        return $this->hasOne(Empleados::class);
    }
}
