<?php

namespace App\Models\Estados;

use App\Models\Empleados\Empleados;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    use HasFactory;

    protected $table = 'estados';

    // Relacion oneToOne 
    public function empleado(){
        return $this->hasOne(Empleados::class);
    }
}
