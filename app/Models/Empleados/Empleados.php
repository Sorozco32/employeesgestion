<?php

namespace App\Models\Empleados;

use App\Models\AreaTrabajo\AreaTrabajo;
use App\Models\Estados\Estados;
use App\Models\Pais\Pais;
use App\Models\TipoDocumento\TipoDocumento;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    use HasFactory;

    protected $table = 'empleados';

    // Relacion oneToOne 
    public function pais_trabajo(){
        return $this->belongsTo(Pais::class);
    }

    // Relacion oneToOne 
    public function tipo_documento(){
        return $this->belongsTo(TipoDocumento::class);
    }

    // Relacion oneToOne 
    public function area_trabajo(){
        return $this->belongsTo(AreaTrabajo::class);
    }

    // Relacion oneToOne 
    public function estado(){
        return $this->belongsTo(Estados::class);
    }
}
