<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use App\Models\AreaTrabajo\AreaTrabajo;
use App\Models\Empleados\Empleados;
use App\Models\Estados\Estados;
use App\Models\Pais\Pais;
use App\Models\TipoDocumento\TipoDocumento;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Empleados::with(['pais_trabajo','estado','tipo_documento','area_trabajo'])->where('id', '>', 0)->get(); 
        return view('empleados/index',compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = Pais::all();
        $estados = Estados::all();
        $tipoDocumento = TipoDocumento::all();
        $areaTrabajo = AreaTrabajo::all();
        $fecha_actual = Carbon::now()->format('Y/m/d H:i:s');
        // dd($paises,$estados,$tipoDocumento,$areaTrabajo,$fecha_actual);
        return view('empleados/create',compact('paises','estados','tipoDocumento','areaTrabajo','fecha_actual'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('data',$request->all() , rand(1,9999) );
        // Validamos el formulario
            $request->validate([
                'primer_apellido' => 'required | regex:/^[A-Z]+$/u | max:20',
                'segundo_apellido' => ' regex:/^[A-Z]+$/u | max:20 ',
                'primer_nombre' => 'required | regex:/^[A-Z]+$/u | max:20',
                // 'otros_nombres' => ' regex:/^[A-Z]+$/u | max:20 ', // error pendiente por resolver
                'pais_empleo_id' => 'required',
                'tipo_documento_id' => 'required',
                'numero_identificacion' => 'required | regex:/^[a-zA-Z0-9]+$/ | max:20',
                'fecha_ingreso' => 'required',
                'estado_id' => 'required',
                'area_trabajo_id' => 'required',
            ]);
        
        //consultar siguiente id empleado
            $id = Empleados::max('id');
            $empleado_id = $id+1;

        // validar que el email no exista en bd
            $existEmail = Empleados::where('email',$emailgenerate)->get();
            if( isset($existEmail) ){
                if( $request->pais_empleo_id == 1 ){
                    $emailgenerate = $request->primer_nombre.'.'.$request->primer_apellido.'.'.$empleado_id.'@cidenet.com.co';
                }else{
                    $emailgenerate = $request->primer_nombre.'.'.$request->primer_apellido.'.'.$empleado_id.'@cidenet.com.us';
                }
            }else{
                if( $request->pais_empleo_id == 1 ){
                    $emailgenerate = $request->primer_nombre.'.'.$request->primer_apellido.'.'.$empleado_id.rand(1,999).'@cidenet.com.co';
                }else{
                    $emailgenerate = $request->primer_nombre.'.'.$request->primer_apellido.'.'.$empleado_id.rand(1,999).'@cidenet.com.us';
                }
            }

             // insertamos en bd
                DB::beginTransaction();
                try {
                    $empleado = new Empleados();
                    $empleado->pais_empleo_id = $request->pais_empleo_id;
                    $empleado->tipo_documento_id = $request->tipo_documento_id;
                    $empleado->area_trabajo_id = $request->area_trabajo_id;
                    $empleado->estado_id = $request->estado_id;
                    $empleado->primer_apellido = $request->primer_apellido;
                    $empleado->segundo_apellido = $request->segundo_apellido;
                    $empleado->primer_nombre = $request->primer_nombre;
                    $empleado->otros_nombres = $request->otros_nombres;
                    $empleado->numero_identificacion = $request->numero_identificacion;
                    $empleado->email = $emailgenerate;
                    $empleado->fecha_ingreso = $request->fecha_ingreso;
                    $empleado->save();

                    DB::commit();

                    $data = true;
                } catch (\Throwable $th) {
                    DB::rollback();
                    $data = false;
                }
        
        if($data){
            return view('empleados/index');
        }else{
            abort(500, 'Could not create office or assign it to administrator');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paises = Pais::all();
        $estados = Estados::all();
        $tipoDocumento = TipoDocumento::all();
        $areaTrabajo = AreaTrabajo::all();
        $fecha_actual = Carbon::now()->format('Y/m/d H:i:s');
        $empleado = Empleados::with(['pais_trabajo','estado','tipo_documento','area_trabajo'])->where('id', $id)->get(); 
        // dd('empleado',$empleado);
        return view('empleados/edit',compact('id','empleado','paises','estados','tipoDocumento','areaTrabajo','fecha_actual'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd( 'update', $id , $request->all());
        $request->validate([
            'primer_apellido' => 'required | regex:/^[A-Z]+$/u | max:20',
            'segundo_apellido' => ' regex:/^[A-Z]+$/u | max:20 ',
            'primer_nombre' => 'required | regex:/^[A-Z]+$/u | max:20',
            // 'otros_nombres' => ' regex:/^[A-Z]+$/u | max:20 ', // error pendiente por resolver
            'pais_empleo_id' => 'required',
            'tipo_documento_id' => 'required',
            'numero_identificacion' => 'required | regex:/^[a-zA-Z0-9]+$/ | max:20',
            'estado_id' => 'required',
            'area_trabajo_id' => 'required',
        ]);

        //consultar siguiente id empleado
            $id_m = Empleados::max('id');
            $empleado_id = $id_m+1;
        
        //validar si el nombre o apellido cambio
            $primer_nombre = Empleados::find($id)->primer_nombre;
            $primer_apellido = Empleados::find($id)->primer_apellido;
            $fecha_ingreso = Empleados::find($id)->fecha_ingreso;

            if( $primer_nombre != $request->primer_nombre || $primer_apellido != $request->primer_apellido ){
                if( $request->pais_empleo_id == 1 ){
                    $emailgenerate = $request->primer_nombre.'.'.$request->primer_apellido.'.'.$empleado_id.rand(1,999).'@cidenet.com.co';
                }else{
                    $emailgenerate = $request->primer_nombre.'.'.$request->primer_apellido.'.'.$empleado_id.rand(1,999).'@cidenet.com.us';
                }

                // validar que el email no exista en bd
                    // $existEmail = Empleados::where('email',$emailgenerate)->get();
            }else{
                $emailgenerate = Empleados::find($id)->email;
            }
            // dd('casi');
            // insertamos en bd
                DB::beginTransaction();
                try {
                    $empleado = Empleados::find($id);
                    $empleado->pais_empleo_id = $request->pais_empleo_id;
                    $empleado->tipo_documento_id = $request->tipo_documento_id;
                    $empleado->area_trabajo_id = $request->area_trabajo_id;
                    $empleado->estado_id = $request->estado_id;
                    $empleado->primer_apellido = $request->primer_apellido;
                    $empleado->segundo_apellido = $request->segundo_apellido;
                    $empleado->primer_nombre = $request->primer_nombre;
                    $empleado->otros_nombres = $request->otros_nombres;
                    $empleado->numero_identificacion = $request->numero_identificacion;
                    $empleado->email = $emailgenerate;
                    $empleado->fecha_ingreso = $fecha_ingreso;
                    // dd('antes del save',$empleado);
                    $empleado->save();

                    DB::commit();

                    $data = true;
                } catch (\Throwable $th) {
                    DB::rollback();
                    $data = false;
                }
    
        if($data){
            return redirect()->route('empleados.consultar');
        }else{
            abort(500, 'Could not create office or assign it to administrator');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado = Empleados::find($id);
        $empleado->delete();
        return redirect()->route('empleados.consultar');
    }
}
