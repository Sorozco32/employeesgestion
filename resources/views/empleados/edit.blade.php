@extends('home')

@section('content')

    <div class="row">
        <div class="content col-md-9" style="margin-top: 60px; margin-left: 230px;">
            <div class="card text-center">
                <h5 class="card-header">Actualizaci&oacute;n del empleado</h5>
                <div class="card-body">

                    <form action="{{ route('empleados.update',$id) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col mb-3">
                                <label for="primer apellido" class="form-label">Primer Apellido</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="primer_apellido" autocomplete="off"  maxlength="20" 
                                    placeholder="Primer Apellido" value="{{ old('primer_apellido', $empleado[0]->primer_apellido) }}">
                                    @error('primer_apellido')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <label for="segundo_apellido" class="form-label">Segundo Apellido</label>
                                <input type="text" class="form-control" name="segundo_apellido" autocomplete="off" maxlength="20" 
                                    placeholder="Segundo Apellido" value="{{ old('segundo_apellido', $empleado[0]->segundo_apellido) }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="primer_nombre" class="form-label">Primer Nombre</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="primer_nombre" autocomplete="off"  maxlength="20" 
                                    placeholder="Primer Nombre" value="{{ old('primer_nombre', $empleado[0]->primer_nombre) }}">
                                    @error('primer_nombre')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <label for="otros_nombres" class="form-label">Otros Nombres</label>
                                <input type="text" class="form-control" name="otros_nombres" autocomplete="off" maxlength="50" 
                                    placeholder="Otros Nombres" value="{{ old('otros_nombres',$empleado[0]->otros_nombres) }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="pais" class="form-label">Pa&iacute;s del empleo</label><span style="color:red">*</span>
                                    <select name="pais_empleo_id" class="form-select"  value="{{ old('pais_empleo_id') }}">
                                        @foreach( $paises AS $pais )
                                            <option value="{{ $pais->id }}"> {{ $pais->nombre }} </option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="col mb-3">
                                <label for="tipo_documento_id" class="form-label">Tipo de identificaci&oacute;n</label><span style="color:red">*</span>
                                    <select name="tipo_documento_id" class="form-select" value="{{ old('tipo_documento_id') }}">
                                        @foreach( $tipoDocumento AS $tdoc )
                                            @foreach( $empleado AS $emp )
                                                @if( $tdoc->id == $emp->tipo_documento_id )
                                                    <option value="{{ $emp->tipo_documento_id }}" selected> {{ $emp->tipo_documento->nombre }} </option>
                                                @else
                                                    <option value="{{ $tdoc->id }}"> {{ $tdoc->nombre }} </option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="numero_identificacion" class="form-label">N&uacute;mero de identificaci&oacute;n</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="numero_identificacion" autocomplete="off"  maxlength="20" 
                                    placeholder="Número de identificación" value="{{ old('numero_identificacion' , $empleado[0]->numero_identificacion) }}">
                                    @error('numero_identificacion')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <label for="fecha_ingreso" class="form-label">Fecha de ingreso</label><span style="color:red">*</span>
                                <input type="date" class="form-control" name="fecha_ingreso" disabled autocomplete="off" value="{{ old('fecha_ingreso', $empleado[0]->fecha_ingreso ) }}">
                                @error('fecha_ingreso')
                                    <br>
                                    <small><span style="color:red">*</span>{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="estado" class="form-label">Estado</label><span style="color:red">*</span>
                                    <select name="estado_id" class="form-select" value="{{ old('estado_id') }}">
                                        @foreach( $estados AS $estado )
                                            @foreach( $empleado AS $emp )
                                                @if( $estado->id == $emp->estado_id )
                                                    <option value="{{ $emp->estado_id }}" selected> {{ $emp->estado->nombre }} </option>
                                                @else
                                                    <option value="{{ $estado->id }}"> {{ $estado->nombre }} </option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </select>
                            </div>
                            <div class="col mb-3">
                                <label for="area_trabajo_id" class="form-label">&Aacute;rea de trabajo</label><span style="color:red">*</span>
                                    <select name="area_trabajo_id" class="form-select" value="{{ old('area_trabajo_id') }}">
                                        @foreach( $areaTrabajo AS $atbo )
                                            @foreach( $empleado AS $emp )
                                                @if( $atbo->id == $emp->area_trabajo_id )
                                                    <option value="{{ $emp->area_trabajo_id }}" selected> {{ $emp->area_trabajo->nombre }} </option>
                                                @else
                                                    <option value="{{ $atbo->id }}"> {{ $atbo->nombre }} </option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="fecha_update" class="form-label">Fecha de edici&oacute;n</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="fecha_update" value="{{ $fecha_actual }}"  disabled>
                            </div>
                            <div class="col mb-3">
                                <br>
                                <button type="submit" class="btn btn-success">Editar</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection 