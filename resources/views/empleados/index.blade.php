@extends('home')

@section('content')

    <div class="row">
        <div class="content col-md-9" style="margin-top: 60px; margin-left: 230px;">
            <div class="card text-center">
                <h5 class="card-header">Registro de empleados</h5>
                <div class="card-body">

                    <table class="table table-hover" id="tablaEmpleados">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">NOMBRES</th>
                                <th scope="col">PRIMER APELLIDO</th>
                                <th scope="col">SEGUNDO APELLIDO</th>
                                <th scope="col">EMAIL</th>
                                <th scope="col">TIPO DOCUMENTO</th>
                                <th scope="col">N&Uacute;MERO DOCUMENTO</th>
                                <th scope="col">AREA TRABAJO</th>
                                <th scope="col">ESTADO</th>
                                <th scope="col">OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $empleados AS $empleado )
                                <tr>
                                    <th> {{ $empleado->id }} </th>
                                    <th> {{ $empleado->primer_nombre }} </th>
                                    <th> {{ $empleado->primer_apellido }} </th>
                                    <th> {{ $empleado->segundo_apellido }} </th>
                                    <th> {{ $empleado->email }} </th>
                                    <th> {{ $empleado->tipo_documento->nombre }} </th>
                                    <th> {{ $empleado->numero_identificacion }} </th>
                                    <th> {{ $empleado->area_trabajo->nombre }} </th>
                                    <th> {{ $empleado->estado->nombre }} </th>
                                    <th>
                                        <a href="{{ route('empleados.edit', $empleado->id) }}"><i class="fa fa-eraser"></i><span>Editar</span></a>
                                        <br>
                                        <!-- <a href="{{ route('empleados.destroy', $empleado->id) }}"><i class="fa fa-trash"></i><span>Eliminar</span></a> -->
                                        <button id="click">click</button>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection 

@section('js')
    <script>
        //pendiente validar porque no funciona el jquery
        $('#tablaEmpleados').DataTable();

        $('#click').click(function(){
            alert('click');
        });
    </script>
@endsection