@extends('home')

@section('content')

    <div class="row">
        <div class="content col-md-9" style="margin-top: 60px; margin-left: 230px;">
            <div class="card text-center">
                <h5 class="card-header">Registro de empleados</h5>
                <div class="card-body">

                    <form action="{{ route('empleados.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col mb-3">
                                <label for="primer apellido" class="form-label">Primer Apellido</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="primer_apellido" autocomplete="off"  maxlength="20" 
                                    placeholder="Primer Apellido" value="{{ old('primer_apellido') }}">
                                    @error('primer_apellido')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <label for="segundo_apellido" class="form-label">Segundo Apellido</label>
                                <input type="text" class="form-control" name="segundo_apellido" autocomplete="off" maxlength="20" 
                                    placeholder="Segundo Apellido" value="{{ old('segundo_apellido') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="primer_nombre" class="form-label">Primer Nombre</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="primer_nombre" autocomplete="off"  maxlength="20" 
                                    placeholder="Primer Nombre" value="{{ old('primer_nombre') }}">
                                    @error('primer_nombre')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <label for="otros_nombres" class="form-label">Otros Nombres</label>
                                <input type="text" class="form-control" name="otros_nombres" autocomplete="off" maxlength="50" 
                                    placeholder="Otros Nombres" value="{{ old('otros_nombres') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="pais" class="form-label">Pa&iacute;s del empleo</label><span style="color:red">*</span>
                                    <select name="pais_empleo_id" class="form-select"  value="{{ old('pais_empleo_id') }}">
                                        @foreach( $paises AS $pais )
                                            <option value="{{ $pais->id }}"> {{ $pais->nombre }} </option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="col mb-3">
                                <label for="tipo_documento_id" class="form-label">Tipo de identificaci&oacute;n</label><span style="color:red">*</span>
                                    <select name="tipo_documento_id" class="form-select" value="{{ old('tipo_documento_id') }}">
                                        @foreach( $tipoDocumento AS $tdoc )
                                            <option value="{{ $tdoc->id }}"> {{ $tdoc->nombre }} </option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="numero_identificacion" class="form-label">N&uacute;mero de identificaci&oacute;n</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="numero_identificacion" autocomplete="off"  maxlength="20" 
                                    placeholder="Número de identificación" value="{{ old('numero_identificacion') }}">
                                    @error('numero_identificacion')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <label for="fecha_ingreso" class="form-label">Fecha de ingreso</label><span style="color:red">*</span>
                                <input type="date" class="form-control" name="fecha_ingreso" autocomplete="off" value="{{ old('fecha_ingreso') }}">
                                @error('fecha_ingreso')
                                    <br>
                                    <small><span style="color:red">*</span>{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="estado" class="form-label">Estado</label><span style="color:red">*</span>
                                    <select name="estado_id" class="form-select" disabled value="{{ old('estado_id') }}">
                                        @foreach( $estados AS $estado )
                                            <option value="{{ $estado->id }}"> {{ $estado->nombre }} </option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="col mb-3">
                                <label for="area_trabajo_id" class="form-label">&Aacute;rea de trabajo</label><span style="color:red">*</span>
                                    <select name="area_trabajo_id" class="form-select" value="{{ old('area_trabajo_id') }}">
                                        @foreach( $areaTrabajo AS $atbo )
                                            <option value="{{ $atbo->id }}"> {{ $atbo->nombre }} </option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label for="fecha_registro" class="form-label">Fecha de registro</label><span style="color:red">*</span>
                                <input type="text" class="form-control" name="fecha_registro"
                                    value="{{ $fecha_actual }}"  disabled>
                                    @error('fecha_registro')
                                        <br>
                                        <small><span style="color:red">*</span>{{ $message }}</small>
                                    @enderror
                            </div>
                            <div class="col mb-3">
                                <br>
                                <button type="submit" class="btn btn-success">Registrar</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection 