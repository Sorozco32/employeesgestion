 
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('home') }}">
        <img src="{{ asset('images/employee1.png') }}" width="50" height="50" >
        Gesti&oacute;n Empleados
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{ route('empleados.registrar') }}">Registrar Empleados</a>
        </li>
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="{{ route('empleados.consultar') }}">Consultar Empleados</a>
        </li>
      </ul>
    </div>
  </div>
</nav>