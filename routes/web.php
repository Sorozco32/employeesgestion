<?php

use App\Http\Controllers\Empleados\EmpleadosController;
use App\Http\Controllers\Homecontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ruta home project
Route::get('/',[Homecontroller::class,'index'])->name('home');

// Rutas empleados
Route::get('/empleados/consultar',[EmpleadosController::class,'index'])->name('empleados.consultar');
Route::get('/empleados/registrar',[EmpleadosController::class,'create'])->name('empleados.registrar');
Route::post('/empleados/store',[EmpleadosController::class,'store'])->name('empleados.store');
Route::get('/empleados/edit/{id}',[EmpleadosController::class,'edit'])->name('empleados.edit');
Route::post('/empleados/update/{id}',[EmpleadosController::class,'update'])->name('empleados.update');
Route::get('/empleados/destroy/{id}',[EmpleadosController::class,'destroy'])->name('empleados.destroy');