<?php

namespace Database\Seeders;

use App\Models\TipoDocumento\TipoDocumento;
use Illuminate\Database\Seeder;

class TiposDocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear registros en la tabla tipo documento
            $tdocumento = new TipoDocumento();
            $tdocumento->nombre = 'Cédula de Ciudadanía';
            $tdocumento->estado = 1;
            $tdocumento->save();

            $tdocumento1 = new TipoDocumento();
            $tdocumento1->nombre = 'Cédula de Extranjería';
            $tdocumento1->estado = 1;
            $tdocumento1->save();

            $tdocumento2 = new TipoDocumento();
            $tdocumento2->nombre = 'Pasaporte';
            $tdocumento2->estado = 1;
            $tdocumento2->save();

            $tdocumento3 = new TipoDocumento();
            $tdocumento3->nombre = 'Permiso Especial';
            $tdocumento3->estado = 1;
            $tdocumento3->save();
    }
}
