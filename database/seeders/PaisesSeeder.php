<?php

namespace Database\Seeders;

use App\Models\Pais\Pais;
use Illuminate\Database\Seeder;

class PaisesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear registros en la tabla pais
            $pais = new Pais();
            $pais->nombre  = 'Colombia';
            $pais->estado = 1;
            $pais->save();

            $pais1 = new Pais();
            $pais1->nombre = 'Estados Unidos';
            $pais1->estado = 1;
            $pais1->save();
    }
}
