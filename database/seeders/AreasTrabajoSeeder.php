<?php

namespace Database\Seeders;

use App\Models\AreaTrabajo\AreaTrabajo;
use Illuminate\Database\Seeder;

class AreasTrabajoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear registros en la tabla area trabajo
            $atrabajo = new AreaTrabajo();
            $atrabajo->nombre = 'Administración';
            $atrabajo->estado = 1;
            $atrabajo->save();

            $atrabajo1 = new AreaTrabajo();
            $atrabajo1->nombre = 'Financiera';
            $atrabajo1->estado = 1;
            $atrabajo1->save();

            $atrabajo2 = new AreaTrabajo();
            $atrabajo2->nombre ='Compras';
            $atrabajo2->estado = 1;
            $atrabajo2->save();

            $atrabajo3 = new AreaTrabajo();
            $atrabajo3->nombre = 'Infraestructura';
            $atrabajo3->estado = 1;
            $atrabajo3->save();

            $atrabajo4 = new AreaTrabajo();
            $atrabajo4->nombre = 'Operación';
            $atrabajo4->estado = 1;
            $atrabajo4->save();

            $atrabajo5 = new AreaTrabajo();
            $atrabajo5->nombre = 'Talento Humano';
            $atrabajo5->estado = 1;
            $atrabajo5->save();

            $atrabajo6 = new AreaTrabajo();
            $atrabajo6->nombre = 'Servicios Varios';
            $atrabajo6->estado = 1;
            $atrabajo6->save();
    }
}
