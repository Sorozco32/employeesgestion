<?php

namespace Database\Seeders;

use App\Models\Estados\Estados;
use Illuminate\Database\Seeder;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear registros en la tabla estado
            $estado = new Estados();
            $estado->nombre = 'Activo';
            $estado->estado = 1;
            $estado->save();

            $estado1 = new Estados();
            $estado1->nombre = 'No Activo';
            $estado1->estado = 1;
            $estado1->save();
    }
}
