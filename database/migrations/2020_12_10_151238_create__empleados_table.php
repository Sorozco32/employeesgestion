<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pais_empleo_id');
            $table->unsignedBigInteger('tipo_documento_id');
            $table->unsignedBigInteger('area_trabajo_id');
            $table->string('primer_apellido', 20);
            $table->string('segundo_apellido', 20)->nullable();
            $table->string('primer_nombre', 20);
            $table->string('otros_nombres', 20)->nullable();
            $table->string('numero_identificacion', 20)->unique();
            $table->string('email', 300)->unique();

            $table->foreign('pais_empleo_id')->references('id')->on('pais_empleo');
            $table->foreign('tipo_documento_id')->references('id')->on('tipo_documento');
            $table->foreign('area_trabajo_id')->references('id')->on('area_trabajo');

            $table->date('fecha_ingreso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_empleados');
    }
}
